import React from 'react';
import './Style.css';
import { Link } from 'react-router-dom';

class Users extends React.Component {
  constructor() {
    super();
    this.state = {
      user: [],
      isDataLoaded: false,
    };
  }

  fetchUserData = async () => {
    try {
      const fetchPost = await fetch(
        'https://jsonplaceholder.typicode.com/posts'
      );
      const post = await fetchPost.json();
      const fetchUser = await fetch(
        'https://jsonplaceholder.typicode.com/users'
      );
      const user = await fetchUser.json();

      this.setState({
        user: user,
        post: post,
        isDataLoaded: true,
      });
    } catch (err) {
      this.setState({ err: 'Data Not Found' });
    }
  };
  componentDidMount() {
    this.fetchUserData();
  }
  getUserName = (usersData, id) => {
    const data = usersData.find((element) => element.id === id);
    return data.username;
  };
  getName = (usersData, id) => {
    const data = usersData.find((element) => element.id === id);
    return data.name;
  };

  render() {
    const { user, post, isDataLoaded } = this.state;
    // console.log('render');
    // console.log(typeof post);
    // console.log(typeof user);

    if (!isDataLoaded) {
      return (
        <div className="loader">
          <h1> Please wait data is loading..... </h1>
        </div>
      );
    } else {
      return (
        <div className="container">
          {post.map((element, index) => (
            <div className="main">
              <div className="userCard">
                <img
                  src="https://www.pngitem.com/pimgs/m/420-4204652_oic-provincial-statistics-officer-psa-maguindanao-user-icon.png"
                  alt=""
                />
                <div className="user">
                  <p className="userName">
                    @{this.getUserName(user, element.userId)}
                  </p>
                  <p className="name">{this.getName(user, element.userId)}</p>
                  <br />
                </div>
              </div>
              <div className="userPost">
                <p className="title">{element.title}</p>
                <p className="body">{element.body}</p>
              </div>
              <div className="comments">
                <Link to={`/comments/${element.id}`} className='link'>
                  <div className="comment">
                    <img
                      src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3fp0XXPGlxiEeFjkHU65U_L6PpMUUTmQJnw&usqp=CAU"
                      alt=""
                      className="comment"
                    />
                    <p>Comments</p>
                  </div>
                </Link>
              </div>
            </div>
          ))}
        </div>
      );
    }
  }
}

export default Users;
