import React from 'react';
import { Link } from 'react-router-dom';
import './App.css';

class Navbar extends React.Component {
  constructor(props) {
    super();
  }

  render() {
    return (
      <div className="navbar">
        <Link to="/" className="nav">
          Home
        </Link>
      </div>
    );
  }
}

export default Navbar;
