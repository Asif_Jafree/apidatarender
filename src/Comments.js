import React from "react";
import './Style.css' ;
export default class Comment extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      isDataLoaded: false,
    };
  }
  componentDidMount() {
    fetch(
      "https://jsonplaceholder.typicode.com/posts/" +
        this.props.match.params.id +
        "/comments"
    )
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          comments: json,
          isDataLoaded: true,
        });
      });
  }
  render() {
    const { isDataLoaded, comments } = this.state;
    if (!isDataLoaded) {
      return (
        <div className="loader">
          <h1> Please wait data is loading.... </h1>
        </div>
      );
    } else {
      return (
        <>
          <h1>Comments</h1>
          <div className="main-comment">
            {comments.map((item) => (
                <div className="comment-card">
                  <div className="heading">
                    <div>
                    <img
                  src="https://www.pngitem.com/pimgs/m/420-4204652_oic-provincial-statistics-officer-psa-maguindanao-user-icon.png"
                  alt=""
                />
                    
                    </div>
                    <div className="title">
                      <p>@{item.name}</p>
                    </div>
                  </div>
                  <div className="body">
                    <p>{item.body}</p>
                  </div>
                </div>
              
            ))}
          </div>
        </>
      );
    }
  }
}
