import React from 'react';
import './App.css';
import { BrowserRouter as Router ,Route,Switch} from 'react-router-dom';
import Users from './Users';
import Comments from './Comments';
import Navbar from './Navbar';

class App extends React.Component {
  constructor(props) {
    super();
    
  }

  render() {
    return (
      <Router>
      <main>
      <Navbar/>
      <Switch>
        <Route path="/" exact component={Users}/>
        <Route path="/comments/:id"   component={Comments}/>
        </Switch>
        </main>
      </Router>
    );
  }
}
export default App;